FROM rocker/rstudio:3.3.1
#install latex for PDF output
RUN apt-get update
RUN apt-get install -y texlive-latex-base texlive-fonts-recommended texlive-latex-extra libcurl4-openssl-dev libxml2-dev
RUN Rscript -e "install.packages('devtools')"
RUN Rscript -e "library(devtools)" -e "install_version('acepack',version = '1.4.1',repos = 'http://cran.us.r-project.org')" \
-e "install_version('acepack', version = '1.3-3.3', repos = 'http://cran.us.r-project.org')" \
-e "install_version('beanplot', version = '1.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('bitops', version = '1.0-6', repos = 'http://cran.us.r-project.org')" \
-e "install_version('chron', version = '2.3-47', repos = 'http://cran.us.r-project.org')" \
-e "install_version('cluster', version = '2.0.4', repos = 'http://cran.us.r-project.org')" \
-e "install_version('codetools', version = '0.2-14', repos = 'http://cran.us.r-project.org')" \
-e "install_version('colorspace', version = '1.2-6', repos = 'http://cran.us.r-project.org')" \
-e "install_version('data.table', version = '1.9.6', repos = 'http://cran.us.r-project.org')" \
-e "install_version('DBI', version = '0.5', repos = 'http://cran.us.r-project.org')" \
-e "install_version('dichromat', version = '2.0-0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('digest', version = '0.6.10', repos = 'http://cran.us.r-project.org')" \
-e "install_version('doParallel', version = '1.0.10', repos = 'http://cran.us.r-project.org')" \
-e "install_version('evaluate', version = '0.9', repos = 'http://cran.us.r-project.org')" \
-e "install_version('foreach', version = '1.4.3', repos = 'http://cran.us.r-project.org')" \
-e "install_version('foreign', version = '0.8-67', repos = 'http://cran.us.r-project.org')" \
-e "install_version('formatR', version = '1.4', repos = 'http://cran.us.r-project.org')" \
-e "install_version('Formula', version = '1.2-1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('GGally', version = '1.2.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('ggplot2', version = '2.1.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('gridExtra', version = '2.2.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('gtable', version = '0.2.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('highr', version = '0.6', repos = 'http://cran.us.r-project.org')" \
-e "install_version('Hmisc', version = '3.17-4', repos = 'http://cran.us.r-project.org')" \
-e "install_version('htmltools', version = '0.3.5', repos = 'http://cran.us.r-project.org')" \
-e "install_version('httpuv', version = '1.3.3', repos = 'http://cran.us.r-project.org')" \
-e "install_version('httr', version = '1.2.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('hwriter', version = '1.3.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('iterators', version = '1.0.8', repos = 'http://cran.us.r-project.org')" \
-e "install_version('knitr', version = '1.14', repos = 'http://cran.us.r-project.org')" \
-e "install_version('labeling', version = '0.3', repos = 'http://cran.us.r-project.org')" \
-e "install_version('lattice', version = '0.20-33', repos = 'http://cran.us.r-project.org')" \
-e "install_version('latticeExtra', version = '0.6-28', repos = 'http://cran.us.r-project.org')" \
-e "install_version('magrittr', version = '1.5', repos = 'http://cran.us.r-project.org')" \
-e "install_version('Matrix', version = '1.2-6', repos = 'http://cran.us.r-project.org')" \
-e "install_version('lattice', version = '0.20-34', repos = 'http://cran.us.r-project.org')" \
-e "install_version('memoise', version = '1.0.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('mime', version = '0.5', repos = 'http://cran.us.r-project.org')" \
-e "install_version('munsell', version = '0.4.3', repos = 'http://cran.us.r-project.org')" \
-e "install_version('nnet', version = '7.3-12', repos = 'http://cran.us.r-project.org')" \
-e "install_version('packrat', version = '0.4.7-1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('pastecs', version = '1.3-18', repos = 'http://cran.us.r-project.org')" \
-e "install_version('pheatmap', version = '1.0.8', repos = 'http://cran.us.r-project.org')" \
-e "install_version('plyr', version = '1.8.4', repos = 'http://cran.us.r-project.org')" \
-e "install_version('RColorBrewer', version = '1.1-2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('Rcpp', version = '0.12.6', repos = 'http://cran.us.r-project.org')" \
-e "install_version('RCurl', version = '1.95-4.8', repos = 'http://cran.us.r-project.org')" \
-e "install_version('reshape', version = '0.8.5', repos = 'http://cran.us.r-project.org')" \
-e "install_version('reshape2', version = '1.4.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('rmarkdown', version = '1.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('rpart', version = '4.1-10', repos = 'http://cran.us.r-project.org')" \
-e "install_version('RSQLite', version = '1.0.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('scales', version = '0.4.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('shiny', version = '0.13.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('stringdist', version = '0.9.4.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('stringi', version = '1.1.1', repos = 'http://cran.us.r-project.org')" \
-e "install_version('stringr', version = '1.0.0', repos = 'http://cran.us.r-project.org')" \
-e "install_version('survival', version = '2.39-5', repos = 'http://cran.us.r-project.org')" \
-e "install_version('tibble', version = '1.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('withr', version = '1.0.2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('XML', version = '3.98-1.4', repos = 'http://cran.us.r-project.org')" \
-e "install_version('xtable', version = '1.8-2', repos = 'http://cran.us.r-project.org')" \
-e "install_version('yaml', version = '2.1.14', repos = 'http://cran.us.r-project.org')"
RUN Rscript -e "source('https://bioconductor.org/biocLite.R')" -e "biocLite()"
RUN Rscript -e "source('https://bioconductor.org/biocLite.R')" -e "biocLite('AnnotationDbi')" \
-e "biocLite('AnnotationHub')" -e "biocLite('Biobase')" -e "biocLite('BiocGenerics')" \
-e "biocLite('BiocParallel')" -e "biocLite('biomaRt')" -e "biocLite('Biostrings')" \
-e "biocLite('biovizBase')" -e "biocLite('BSgenome')" -e "biocLite('ensembldb')" \
-e "biocLite('GenomeInfoDb')" -e "biocLite('GenomeInfoDb')" -e "biocLite('GenomicAlignments')" \
-e "biocLite('GenomicFeatures')" -e "biocLite('GenomicRanges')" -e "biocLite('ggbio')" \
-e "biocLite('graph')" -e "biocLite('Gviz')" -e "biocLite('interactiveDisplayBase')" \
-e "biocLite('IRanges')" -e "biocLite('OrganismDbi')" -e "biocLite('RBGL')" \
-e "biocLite('Rsamtools')" -e "biocLite('rtracklayer')" -e "biocLite('S4Vectors')" \
-e "biocLite('ShortRead')" -e "biocLite('SummarizedExperiment')" -e "biocLite('VariantAnnotation')" \
-e "biocLite('XVector')" -e "biocLite('zlibbioc')"
RUN Rscript -e "library(devtools)" -e "install_version('matrixStats', version = '0.50.2', repos = 'http://cran.us.r-project.org')"
WORKDIR /root
#install samtools
RUN apt-get install -y lbzip2 libncurses5-dev libncursesw5-dev curl
RUN curl http://heanet.dl.sourceforge.net/project/samtools/samtools/1.3.1/samtools-1.3.1.tar.bz2 -o /root/samtools-1.3.1.tar.bz2
RUN tar -xf samtools-1.3.1.tar.bz2
WORKDIR /root/samtools-1.3.1
RUN make
RUN make install
RUN rm /root/samtools-1.3.1.tar.bz2
#install BBmap
WORKDIR /home/rstudio
RUN apt-get install -y openjdk-7-jre-headless
RUN curl http://heanet.dl.sourceforge.net/project/bbmap/BBMap_36.64.tar.gz | tar xvz
#install starcode
RUN git clone git://github.com/gui11aume/starcode.git
WORKDIR /home/rstudio/starcode
RUN make
RUN ln -s /home/rstudio/starcode/starcode /usr/bin/starcode
WORKDIR /home/rstudio
#install bowtie2
RUN apt-get install -y bowtie2
#Adding the scripts and environment files
ADD ./config.txt ./workflowlibrary4.Rproj ./libraryWorkflow.R ./
RUN mkdir functions adapters adapters/pE17-VP2_afterCre bowtieIndices
ADD ./functions/buildFragTableSC3.R ./functions/
ADD ./adapters/pE17-VP2_afterCre/BC-L.fa ./adapters/pE17-VP2_afterCre/BC-R.fa ./adapters/pE17-VP2_afterCre/Ltrim.fa ./adapters/pE17-VP2_afterCre/Rtrim.fa ./adapters/pE17-VP2_afterCre/uncut.fa ./adapters/pE17-VP2_afterCre/
ADD ./bowtieIndices/WGA.1.bt2  ./bowtieIndices/WGA.3.bt2  ./bowtieIndices/WGA.fasta ./bowtieIndices/WGA.rev.2.bt2 ./bowtieIndices/WGA.2.bt2 ./bowtieIndices/WGA.4.bt2 ./bowtieIndices/WGA.rev.1.bt2 ./bowtieIndices/
RUN chown -R rstudio:rstudio /home/rstudio/*
