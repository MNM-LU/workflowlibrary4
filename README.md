# README #

This repository contains the R-scripts and configurationfiles required to replicate the data analysis for figure 7 and 8 of Davidsson et al., 2016 "Novel process of viral vector barcoding and library preparation allows for high-diversity library generation and recombination free paired-end sequencing"


### What is this repository for? ###

* Quick summary
This is a example script to demonstrate the required data analysis workflow when generating and analysing barcoded plasmid libraries. While is provides the sufficient flexibility to analyse all libraries presented in the paper, it is not guaranteed to work with other libraries without modifications. 

To run, download the sequencing files into a local directory ("~/rawData") from [SRR3473446](https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?run=SRR3473446)
then using docker run the following command:


```
#!sh

docker run -v ~/rawData:/home/rstudio/rawData -i -p 9797:8787 --name workflowlibrary4 bjorklund/workflowlibrary4:v0.3


```
In this command, the "~/rawData" can be replaced with the local directory where  the sequencing files are downloaded and the "9797" to a suitable port where to contact rStudio server. 

then log onto the webserver [http://localhost:9797](http://localhost:9797)
The username and password are both "rstudio"


* Version 0.3

### How do I get set up? ###

* Summary of set up

Clone the Bitbucket repository using git into a local directory. Generate the docket image and download the sequencing files. 

* Configuration

No configuration above the installation is required. All dependencies should be included during the Docker image generation. The default is to analyse the entire dataset using 16 threads however, both these factors can be changed in the config.txt file.

* Dependencies
As this version is do generated containing a Dockerfile, it should be possible to install on any platform with minimal software installed. It in known to function on Ubuntu Linux 14.04LTS (64bit) an MacOS X 10.11 El Capitan but should work on and Docker compatible platform.

The main executable script has been validated in R version 3.3.1 with the R packages listed in the Docker file (Automatically downloaded and installed during the docker image build. It is not compatible with R version 2.2 anymore due to changes in the script required by a change in the Data Frame function. For Standalone installation outside Docker, please use the v0.2. For backwards compatibility, please use the version v0.1 (a git Tag) of the script. 
 
* Database configuration

The FastQ files from the sequencing utilized in the manuscript are available publicly from NCBI Sequence Read Archive (SRA) with the Accession numbers:[PRJNA320207](https://www.ncbi.nlm.nih.gov/Traces/study/?acc=SRP074313&go=go)

To regenerate the plots found in figure 7, download the two following FastQ files and place them a local directory. Here re assume that this directory is called "rawData". 

To replicate the analysis in the paper from figure 7, please download the two files for [SRR3473446](https://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?run=SRR3473446) into the rawData directory and name them:
```
#!sh
160809_S7_NextSeq150x150_S5-6_MiSeq150x150_WGAlib_Cre_shortPCR_mixedPCR_PreCR_1.fastq.gz
160809_S7_NextSeq150x150_S5-6_MiSeq150x150_WGAlib_Cre_shortPCR_mixedPCR_PreCR_2.fastq.gz

```

If the name is changed of these files, then update the config.txt file accordingly. Then place them in the rawData directory.

### * How to run tests ###

**Step 1,  Download and run the Docker container:**

```
#!sh

docker run -v ~/rawData:/home/rstudio/rawData -i -p 9797:8787 --name workflowlibrary4 bjorklund/workflowlibrary4:v0.3

```
"~/rawData" corresponds to the local directory where the sequencing data is stored. the 9797 port number can be changed to any port suitable if any other rstudio server instance is running locally. 

**Step 3, Log onto the Docker instance of rStudio:**
Use any web browser and insert [http://localhost:9797](http://localhost:9797)
The username and password are both "rstudio"


**Step 4, Run the analysis:**

This Script is designed to be executed and formatted by Knitr. Therefore utilize "Compile Notebook" in RStudio on the "libraryWorkflow.R


### Example output ###

A complete execution of the workflow and the corresponding data can be found in the "libraryWorkflow.pdf" document. This is a raw, unmodified output from Knitr

### Who do I talk to? ###

* Repo owner or admin

Thes software is written by the Molecular Neuromodulation team under the leadership of A/Prof Tomas Bjorklund. Please visit our [Homepage](http://www.neuromodulation.se) for contact information.